/**
 * Bai 1
 * input: do dai 2 canh goc vuong
 * 

 * step:
 * + Step1: tao 2 bien chua 2 canh goc vuong
 * + Step2: tao bien chua ket qua can tim
 * + Step3: ap dung cong thuc pytago
 * 
 
 * output
 */
var edge1 = 3;
var edge2 = 4;
var result = null;
result = edge1 * edge1 + edge2 * edge2;
console.log('result: ', result);
result = Math.sqrt(result);
console.log('result: ', result);



/**
 * Bai 2
 * 
 * 123  =>  6
 * 
 * 246  =>  12
 * 
 */
var number = 246;

var donVi = number % 10;

var chuc = Math.floor(number / 10) % 10;

var tram = Math.floor(number /100);

var result2 = donVi + chuc + tram;
console.log('result2: ', result2);


// Bài Về Nhà

/**
 * Bai 1 Tính lương nhân viên
 * 
 * Step 1: Khai báo biến số ngày làm
 * 
 * Step 2: kHai báo biến số tiền lương 100k
 * 
 * Step 3: Lập công thức tính lương = số ngày làm * 100k/ ngày
 * 
 */
var workDay = null;
workDay = 360;
var salaryPerday = 100000;
var salaryPaid = workDay * salaryPerday;
console.log('salaryPaid: ', salaryPaid);

/**
 * Bài 2 Tính giá trị trung bình
 * 
 * Step 1: Khai báo biến số thực, tổng cọng có 5 biến số thực
 * => khai báo 5 var cho 5 biến số
 * 
 * Step 2: khai báo biến kết quả tính trung bình. (mean = trung bình)
 */
var numb01 = null;
numb01 = 5;
var numb02 = null;
numb02 = 5;
var numb03 = null;
numb03 = 10;
var numb04 = null;
numb04 = 15;
var numb05 = null;
numb05 = 20;
var resultMean = (numb01 + numb02 + numb03 + numb04 + numb05) / 5;
console.log('resultMean: ', resultMean);
// để tính toán ta sẽ update ngẫu nhiên các số thực vô biến

/**
 * Bài 3 Quy đổi tiền
 * 
 * Step 1: Khai báo biến tỷ giá đô/vnd (exchange rate Exrate)
 * 
 * Step 2: Khai báo biến USD (số tiền)
 * 
 * Step 3: khai báo biến kết quả = biến step 1 * step 2
 */
var Exrate = 23500;
var USD = null;
// ta nhập lượng tiền cần quy đổi
USD = 4000;
var resultExrate = Exrate * USD;
console.log('resultExrate: ', resultExrate);

/**
 * Bài 4 tính chu vi và diện tích hình chữ nhật
 * 
 * Step 1: khai báo biến dài và biến rộng
 * Dài = length
 * Rộng = width
 * 
 * Step 2: khai báo biến kết quả tính chu vi và diện tích
 * chu vi = perimiter
 * diện tích = area
 * 
 * Step 3: lập công thức tính và khai báo biến kết quả log
 * 
 */
var length = null;
length = 5;
var width = null;
width = 3;
var area = length * width;
console.log('area: ', area);
var perimiter = ( length + width ) * 2;
console.log('perimiter: ', perimiter);

/**
 * Bài 5 Tổng 2 ký số
 * 
 * Step 1:đặt ngẫu nhiên 1 con ký số lấy số 69 (nice)
 * 
 * Step 2:
 * a/ tạo biến số giá trị 2 chữ số 
 * b/ tạo biến tìm giá trị hàng chục = 69/10, dùng math.floor để lấy số chẵn
 * c/ tạo biến tìm giá trị hàng đơn vị = công thức lấy dư %
 * 
 * Step 3: tạo biến tổng 2 ký số = biến chục + biến đơn vị và in kết quả
 */
var number = 69;
var soChuc = Math.floor(number / 10);
var soDonvi = number % 10;
var kqKyso = null;
kqKyso = soChuc + soDonvi;
console.log('kqKyso: ', kqKyso);





